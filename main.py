import socket
from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

#Creamos una instancia de FastAPI
app = FastAPI()

#Cambiar titulo del doc generado
app.title = "Mi aplicación con  FastAPI"
app.version = "0.0.1"

#Cap: 26 Manejo de errores y middlewares en FastAPI
#En get_movies, adrede cometemos un error de sintaxis
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

#Para obtener toda la información de los objetos y los modelos mapeados
Base.metadata.create_all(bind=engine)

#Cap 18: Middlewares de autenticación en FastAPI
#Cap 26: Este segmento de código pasa al archivo jwt_bearer.py
#class JWTBearer(HTTPBearer):
    #solicita el token al usuario
#    async def __call__(self, request: Request):#__call__, está dentro de http.py
        #Obtenemos datos de las credenciales
#        auth = await super().__call__(request)#super() clase superior-->HTTPBearer
#        data = validate_token(auth.credentials)
#        if data['email'] != "admin@gmail.com":
#            raise HTTPException(status_code=403, detail='Las credenciales son invalidas')
    

    
        
movies = [
    {
		"id": 1,
		"title": "Avatar",
		"overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
		"year": "2009",
		"rating": 7.8,
		"category": "Acción"
	},
    {
		"id": 2,
		"title": "Avatar",
		"overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
		"year": "2009",
		"rating": 7.8,
		"category": "Acción"
	}
]

def obtener_ip():
    nombre_host = socket.gethostname()
    ip_local = socket.gethostbyname(nombre_host)
    return ip_local
    

#Cremos el primer EndPoint
@app.get('/' , tags=['home'])#cambiar default por tags=['home']
def message():
    return HTMLResponse('<h1>Hello world</h1>')#return "Hello world!"
   
#Para ejecutar la aplicación
#escribir en la terminal lo siguiente:
#1 uvicorn main:app --reload
#2 uvicorn main:app --reload --port 5000 ó 8000
#Uvicorn es un servidor ASGI (Asynchronous Server Gateway Interface)
# que se utiliza comúnmente con FastAPI y otros frameworks web asincrónicos de Python
#De chatGPT: uvicorn nombre_de_tu_modulo:app --host 0.0.0.0 --port 8000
# Si necesitamos ejecutar desde otro dispositivo en la red:
#uvicorn main:app  --reload --port 5000 --host 0.0.0.0

#En la taerminal: python ./main.py
if __name__  == '__main__':
    ip = obtener_ip()
    print('IP************',ip)
