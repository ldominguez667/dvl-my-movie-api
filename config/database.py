import os
from sqlalchemy import create_engine, text
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base #para manipular todas las tablas
from sqlalchemy.exc import OperationalError

#Definir variavles de la BD:
USER_DB = 'postgres'
PASS_DB = 'Jarasho*0667'
URL_DB  = 'localhost:5432'
NAME_BD =  'movie'
# Configuración de la base de datos
#1
#SQLALCHEMY_DATABASE_URI = "postgresql://postgres:Jarasho*0667@localhost:5432/pythondb"
#2
SQLALCHEMY_DATABASE_URI = f"postgresql://{USER_DB}:{PASS_DB}@{URL_DB}/{NAME_BD}"
#Para evitar modificaciones para un mejor rendimiento
SQLALCHEMY_TRACK_MODIFICATIONS = False

engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=True)

SessionLocal = sessionmaker(bind=engine)

Base = declarative_base()

