
from pydantic import BaseModel, Field
from typing import Optional, List

#Esquema de datos
class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=5, max_length=15)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=2022)#>=1 y <=10
    rating:float = Field(ge=1, le=10)
    category:str = Field(min_length=5, max_length=15)
    
    #https://fastapi.tiangolo.com/es/tutorial/schema-extra-example/?h=json+schema#examples-in-json-schema-openapi
    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "id": 1,
                    "title": "MiPelicula",
                    "overview": "MiDescripcion",
                    "year": 2022,
                    "rating": 9.8,
                    "category": "action"
                }
            ]
        }
    }