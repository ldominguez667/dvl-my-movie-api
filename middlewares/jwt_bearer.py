from fastapi.security import HTTPBearer
from fastapi import Request, HTTPException
from utils.jwt_manager import create_token, validate_token

class JWTBearer(HTTPBearer):
    #solicita el token al usuario
    async def __call__(self, request: Request):#__call__, está dentro de http.py
        #Obtenemos datos de las credenciales
        auth = await super().__call__(request)#super() clase superior-->HTTPBearer
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail='Las credenciales son invalidas')