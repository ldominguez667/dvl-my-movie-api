from fastapi import APIRouter
from fastapi import Depends, Path, Query,status
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import SessionLocal
from sqlalchemy.orm import Session
from models.movie import Movie as MovieModel #Para que el nombre no coincida con la clase Movie
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()

def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

#Clase 10: Creación de esquemas con Pydantic

#Filtra todas las películas
#Cap 14.  HTTP 200 OK indica que la solicitud ha tenido éxito.
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies(db:Session=Depends(get_db)) -> List[Movie]:
    #db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


#Filtra la película por Id
@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)
def get_movie(id: int=Path(ge=1, le=2000),db:Session=Depends(get_db))-> Movie:#Cap 10 id,Validaciones de parámetros con Pydantic: int=Path(ge=1, le=2000)
    #db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=status.HTTP_200_OK, content=jsonable_encoder(result))

#Filtra la película por categoría
#Cap 7: Parámetros Query en FastAPI
@movie_router.get('/movies/',  tags=['movies'],response_model=List[Movie])
def get_movies_by_category(category :str=Query(min_length=5, max_length=15),db:Session=Depends(get_db))-> List[Movie]:#category :str, parámetro query
    #Cap 22 Consulta de datos con SQLAlchemy
    #db = Session()
    result = MovieService(db).get_movies_by_category(category)
    if not result:
    #Reto Cap7: Filtrado de las películas by category
        #data = [ item for item in movies if item['category'] == category ]
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    #return JSONResponse(content=data)
    return  result
	#Solución del Teacger:
    
#Cap 8, Método POST en FastAPI
#@app.post('/movies', tags=['movies'])
#Contenido del Body, para que pertenezca al contenido de la petición
#def create_movie(id: int = Body(), title: str = Body(), overview:str = Body(), year:int = Body(), rating: float = Body(), category: str = Body()):
 #   movies.append({
 #       "id": id,
 #       "title": title,
 #       "overview": overview,
 #       "year": year,
 #       "rating": rating,
 #       "category": category
 #   })
 #   return movies

#Clase 10: Creación de esquemas con Pydantic
#Remplazamos método POST del Cap 8 por:
#Cap 14:  201 significa que una solicitud se procesó correctamente y devolvió,o creó, un recurso o resources en el proceso
@movie_router.post('/movies', tags=['movies'], response_model=dict)
def create_movie(movie: Movie,db:Session=Depends(get_db))-> dict:#Voy a requrir una película movie: de tipo Movie
    #Cap 22 crear unas sesión para conectarnos a la base de datos
    #db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la película"})
    #db.close()
    #movies.append(movie)
    # sin Base de datos: movies.append(movie.model_dump())
    #return movies
    #return JSONResponse(status_code=status.HTTP_201_CREATED, content={"message": "Se ha registrado la película"})

#Cap 8: Reto, Método PUT en FastAPI, Modificación.
'''@app.put('/movies/{id}', tags=['movies'])#{id} se requiere el parámetro de ruta.
def update_movie(id: int, title: str = Body(), overview:str = Body(), year:int = Body(), rating: float = Body(), category: str = Body()):
	for item in movies:
		if item["id"] == id:
			item['title'] = title,
			item['overview'] = overview,
			item['year'] = year,
			item['rating'] = rating,
			item['category'] = category
			return movies'''
   
#Clase 10: Creación de esquemas con Pydantic
#Remplazamos método PUT del Cap 8 por:
@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)#{id}, parametro ruta.
def update_movie(id: int, movie: Movie,db:Session=Depends(get_db))-> dict:
    #db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la película"})
    #Remplazamos método PUT del Cap 8 por:
	#for item in movies:
	#	if item["id"] == id:
	#		item['title'] = movie.title
	#		item['overview'] = movie.overview
	#		item['year'] = movie.year
	#		item['rating'] = movie.rating
	#		item['category'] = movie.category
	#		return JSONResponse(status_code=200,content={"message": "Se ha modificado la película"}) 
            #return movie


#Cap 8: Reto, Método DELETE en FastAPI
@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict)
def delete_movie(id: int,db:Session=Depends(get_db))-> dict:
    #db = Session()
    result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se encontró"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=status.HTTP_200_OK, content={"message": "Se  eliminó la película"})
   

    #for item in movies:
     #   if item["id"] == id:
      #      movies.remove(item)
       #     return JSONResponse(status_code=200, content={"message": "Se ha eliminado la película"}) 
            #return movies

#Cap  14 : Reto El error 404, que también se muestra como 404 Not Found